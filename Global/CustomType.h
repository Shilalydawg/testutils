#ifndef TESTUTILS_GLOBAL_CUSTOMTYPE_H
#define TESTUTILS_GLOBAL_CUSTOMTYPE_H

class CustomType
{
private:

    int instance_num;

public:

    CustomType(int instance_num_in);

    inline int get_instance_num();
};

inline int CustomType::get_instance_num()
{
    return instance_num;
}

#endif